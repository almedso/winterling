//! #  Winterling
//!
//! Winterling is a service that exposes an minimal REST-ful API of
//! IoT (linux based) device.
//!
//! * query hw and software identifiers/versions
//! * basic access control (viewer/administrator users)
//! * set and monitor device state (idle, active, standby)
//! * instruct operations
//! * query environment values and sensor values
//!
//! # Usage
//!
//! Call
//! ```no_run
//! winterling --help
//! ```
//! to understand the command line parameters.
//! Explore config module to learn about file based configuration of the service.
//!
//! # Development
//!
//! Winterling uses the *tide* framework to expose a REST-ful API.
//! The file *main.rs* is the entry point of the winterling
//! main.rs  uses cli.rs and config.rs to obtain configuration from file
//! and command line parameters.
//!
//! The module *rest* contains the complete api - any api extension goes there.

mod misc {
    use tide::log::LevelFilter;
    use serde::Deserialize;

    #[derive(Deserialize)]
    #[serde(remote = "LevelFilter")]
    pub enum LevelFilterDef {
        Off,
        Error,
        Warn,
        Info,
        Debug,
        Trace,
    }

    pub fn into_loglevel(level: &str) -> LevelFilter  {

        match level {
            "off" => LevelFilter::Off,
            "error" => LevelFilter::Error,
            "warn" => LevelFilter::Warn,
            "info" => LevelFilter::Info,
            "debug" => LevelFilter::Debug,
            "trace" => LevelFilter::Trace,
            _ => LevelFilter::Error
        }
    }
}

#[doc(hidden)]
pub mod cli;
pub mod rest;
pub mod config;