use log::{error, warn, info};
use serde::{Serialize, Deserialize};
use std::clone::Clone;
use std::marker::{Send, Sync};
use tide::prelude::*;
use tide::{Request, Body, Response};

use super::authenticator::User;

#[derive(Serialize, Debug)]
pub struct DeviceInfoResponse {
    identifier: String,
    serial_number: String,
    host: String,
    state: String,
}


#[derive(Deserialize, Serialize, Debug)]
pub struct Setting {
    property: String,
    value: String,
}

pub async fn get_info<State: Clone + Send + Sync + 'static>(_req: Request<State>) -> tide::Result {
    let device_info = DeviceInfoResponse {
        identifier: "from-config".into(),
        serial_number: "from-config".into(),
        host: "localhost".into(),
        state: "idle".into(),
    };
    let mut res = Response::new(200);
    res.set_body(json!(&device_info));
    Ok(res)
}

pub async fn get_settings<State: Clone + Send + Sync + 'static>(req: Request<State>) -> tide::Result {
    let settings =  [
        Setting { property: "language".to_owned(), value: "de".to_owned() },
        Setting { property: "auth".to_owned(), value: "true".to_owned() }
    ];
    let mut res = Response::new(200);
    res.set_body(json!(&settings));
    Ok(res)
}

pub async fn post_set<State: Clone + Send + Sync + 'static>(mut req: Request<State>) -> tide::Result<tide::Response> {
    let mut response = tide::Response::new(tide::http::StatusCode::Ok);
    if let Some(user) = req.ext::<User>() {
        let setting = req.body_json::<Setting>().await?;
        info!("Set property {} to value {}", setting.property, setting.value);
        response.set_body(Body::from_json(&setting)?);
    } else {
        response.set_status(tide::http::StatusCode::Unauthorized);
        response.insert_header("WWW-Authenticate", "Basic");
    }
    Ok(response)
}
