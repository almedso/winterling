use std::future::Future;
use std::pin::Pin;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;

use tide::{Middleware, Next, Request, Response, Result, StatusCode};

use super::state::{AppState, RequestCount};

// This is an example of a function middleware that uses the
// application state. Because it depends on a specific request state,
// it would likely be closely tied to a specific application

pub fn user_loader<'a>(
    mut request: Request<AppState>,
    next: Next<'a, AppState>,
) -> Pin<Box<dyn Future<Output = Result> + Send + 'a>> {
    Box::pin(async {
        if let Some(user) = request.state().find_user().await {
            tide::log::trace!("user loaded", {user: user.name});
            request.set_ext(user);
            Ok(next.run(request).await)
        // this middleware only needs to run before the endpoint, so
        // it just passes through the result of Next
        } else {
            // do not run endpoints, we could not find a user
            Ok(Response::new(StatusCode::Unauthorized))
        }
    })
}


// This is an example of middleware that keeps its own state
// is selfcontained and therefore
// could be provided as a third party crate
#[derive(Default)]
pub struct RequestCounterMiddleware {
    requests_counted: Arc<AtomicUsize>,
}

impl RequestCounterMiddleware {
    pub fn new(start: usize) -> Self {
        Self {
            requests_counted: Arc::new(AtomicUsize::new(start)),
        }
    }
}

#[tide::utils::async_trait]
impl<State: Clone + Send + Sync + 'static> Middleware<State> for RequestCounterMiddleware {
    async fn handle(&self, mut req: Request<State>, next: Next<'_, State>) -> Result {
        let count = self.requests_counted.fetch_add(1, Ordering::Relaxed);
        tide::log::debug!("request counter {}", count );
        req.set_ext(RequestCount(count));
        let mut res = next.run(req).await;
        res.insert_header("request-number", count.to_string());
        Ok(res)
    }
}
