// The application state
use std::marker::{Send, Sync};

pub struct RequestCount(pub usize);

#[derive(Debug)]
pub struct User {
    pub name: String,
}

#[derive(Clone, Default, Debug)]
pub struct AppState;

impl AppState {
    pub async fn find_user(&self) -> Option<User> {
        Some(User {
            name: "nori".into(),
        })
    }
}

// just to be safe it is there - might be removed later on
unsafe impl Send for AppState {}
unsafe impl Sync for AppState {}