
use std::collections::HashMap;
use tide::{Request, Response};
use serde_json::json;
use serde::{Serialize};
use tide_http_auth::{BasicAuthRequest, Storage};
use google_authenticator::GoogleAuthenticator;

use tide::log::{info, debug};

/// we use google authenticator since it allows
///
/// * Run http without ssl encrpytion
/// * never ever transmit the key in plain text to the client
///
#[derive(Serialize, Clone)]
pub struct User {
    pub username: String,
    authenticator_key: String,
}

impl User {
    pub fn new(username: String ) -> Self {
        let auth = GoogleAuthenticator::new();
        // use a very short secret since security approach is not high
        let  authenticator_key = auth.create_secret(8);
        User { username, authenticator_key }
    }
}

#[derive(Serialize, Clone)]
pub struct AccessState {
    users: HashMap<String, User>,
}

impl AccessState {
    pub fn default() -> Self {
        debug!("Initialize default AccessState");
        // we operate with two fixed users
        let userlist = vec![
            User::new("administrator".to_string()),
            User::new("viewer".to_string()),
        ];

        let mut users = HashMap::new();
        for user in userlist {
            users.insert(user.username.to_owned(), user);
        }

        AccessState { users }
    }
}


pub async fn get_users(req: Request<AccessState>) -> tide::Result {
    debug!("Received get_users request");
    let mut res = Response::new(200);
    res.set_body(json!(req.state()));
    Ok(res)
}


#[tide::utils::async_trait]
impl Storage<User, BasicAuthRequest> for AccessState {
    async fn get_user(&self, request: BasicAuthRequest) -> tide::Result<Option<User>> {
        debug!("Authenticate request - username: {}", &request.username);
        match self.users.get(&request.username) {
            Some(user) => {
                let auth = GoogleAuthenticator::new();
                if auth.verify_code(&user.authenticator_key, &request.password, 1, 0) {
                    info!("User authentication failed - username: {}", &request.username);
                    return Ok(None);
                }
                info!("User authenticated - username: {}", &request.username);
                Ok(Some(user.clone()))
            }
            None => {
                info!("User not found - username: {}", &request.username);
                Ok(None)
            },
        }
    }
}
