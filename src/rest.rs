//! REST-ful API Definitions
//!
//! TODO
//!
//! * Decide on how to document either via rustdoc or alternatively swagger

// # Implementation Note
//
// This file mainly includes all REST- api functions and middleware
// and just conducts url endpoints and middleware.

use middleware::{RequestCounterMiddleware};
use tide::{Server};
use tide_http_auth::{Authentication, BasicAuthScheme};

pub mod state;

mod authenticator;
mod device;
mod middleware;

use authenticator::{AccessState, get_users};


pub fn app() -> Server<AccessState>  {

    // create the app with state
    let mut app = tide::with_state(AccessState::default());

    // inject the middleware
    app.with(Authentication::new(BasicAuthScheme::default()));
    app.with(RequestCounterMiddleware::new(0));

    // configure routes: core device info
    app.at("/device/info").get(device::get_info);
    app.at("/device/settings").get(device::get_settings);
    app.at("/device/set").post(device::post_set);

    app.at("/auth/users").get(get_users);
    app
}
