//! Service Configuration via File
//!
//! Config file format is yaml.
//!
//! The config file name can be given via cli option.
//! If no option is given, is is tried to open the default config file named
//! */etc/winterling/config.yml*
//!
//! The config file is optional, if not given
//! or not available the default content is used:
//! ```no_run
//! ---
//! # winterling configuration in yml
//! host: "localhost"
//! port: 4242
//! log_level: Info
//! ```
//! Configuration parameters and types explained inside to the config struct

use serde::Deserialize;
use serde_yaml::from_str;
use std::fs::File;
use std::io::Read;
use tide::log::LevelFilter;


use crate::cli;
use super::misc::LevelFilterDef;


#[derive(Deserialize, Debug)]
pub struct Config {
    /// The host to listen on
    /// Can be given as hostname or as ip address string
    /// (use 0.0.0.0 on all ip addresses of the port)
    ///
    /// Default is "localhost"
    pub host: String,
    /// The port to listen on
    ///
    /// Default is 4242
    pub port: u16,
    /// The log level to use
    ///
    /// Default is INFO
    #[serde(with = "LevelFilterDef")]
    pub log_level: LevelFilter,
}

impl Default for Config {
    fn default() -> Self {
        Config {
            host: String::from("localhost"),
            port: 4242,
            log_level: LevelFilter::Info,
        }
    }
}

impl Config {
    /// Load config using path specified in options
    pub fn load(opts: &cli::Options) -> Result<Config, Box<dyn(::std::error::Error)>> {
        // Read file to string
        let mut raw = String::new();
        match File::open(&opts.config_path) {
            Ok(mut f) => {
                f.read_to_string(&mut raw)?;
                // Parse as yaml
                from_str(&raw).map_err(From::from)
            },
            Err(_err) => Ok(Config::default())
        }

    }
}
