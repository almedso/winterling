
extern crate clap;

use tide::log::LevelFilter;
use std::path::PathBuf;
use clap::{Arg, App};

use super::misc::into_loglevel;

#[derive(Debug)]
pub struct Options {
    /// Path to the configuration file to use
    pub config_path: PathBuf,

    pub log_level: Option<LevelFilter>,
}

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

impl Options {

    pub fn load() -> Options {
        let matches = App::new("winterling")
            .version( VERSION )
            .author("Volker Kempert <volker.kempert@almedso.de>")
            .about("winterling is a nice flower and a synonym for this service")
            // -c, --config
            .arg(Arg::with_name("config")
                .short("c")
                .long("config")
                .value_name("FILE")
                .help("Sets a custom config file")
                .takes_value(true))
            .arg(Arg::with_name("level")
                .short("l")
                .long("log")
                .takes_value(true)
                .possible_values(&["off", "error", "warn", "info", "debug", "trace"])
                .help("Overwrite loglevel from config file or from default (i.e. error)"))
            .get_matches();

        // Gets a value for config if supplied by user, or provides default.
        let config_path = PathBuf::from(matches.value_of("config")
            .unwrap_or("/etc/winterling/config.yml"));
        let log_level: Option<LevelFilter> = match  matches.value_of("level") {
            Some(level) => Some(into_loglevel(level)),
            _ => None,
        };
        Options {
            config_path,
            log_level,
        }
    }
}
