use log::{error, warn, info, debug, trace};

use env_logger::Builder;

use winterling::{ cli, config::Config, rest };

const APPLICATION: &'static str = env!("CARGO_PKG_NAME");
const VERSION: &'static str = env!("CARGO_PKG_VERSION");

async fn async_run(config: &Config) -> Result<(), Box<dyn(::std::error::Error)>> {
    info!("Starting {} at version {}", APPLICATION, VERSION );
    #[cfg(debug_assertions)]
    {
        error!("LogLevelTest: Output ERROR");
        warn!("LogLevelTest: Output WARN");
        info!("LogLevelTest: Output INFO");
        debug!("LogLevelTest: Output DEBUG");
        trace!("LogLevelTest: Output TRACE");
    }
    let app = rest::app();
    let srv_uri = format!("{}:{}", config.host, config.port);
    info!("Application listens at {}", srv_uri);
    app.listen(srv_uri).await?;
    info!("Application stopped");
    Ok(())
}

#[async_std::main]
async fn main() -> Result<(), Box<dyn(::std::error::Error)>> {
    // Load command-line options
    let opts = cli::Options::load();

    // Load configuration
    let config = Config::load(&opts)?;

    // Initialize logging
    let mut builder = Builder::from_default_env();

    // figure out the log level
    let log_level = match opts.log_level {
        Some(level) => level,
        None => config.log_level
    };

    builder.filter(None, log_level)
        // this does not work in the context of async_std::main
        //.format(|buf, record| writeln!(buf, "{} - {}", record.level(), record.args()))
        .init();
    info!("Set LogLevel to {}", log_level);

    // run the application
    async_run(&config).await
}
